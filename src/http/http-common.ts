import axios from 'axios';

export const HTTP = axios.create({
  // baseURL: `http://laptop-no5q4vbm:8082/`,
  // baseURL: `http://laptop-c2k557gi:8080/`,
  baseURL: `http://esslite-app.herokuapp.com/`,
  headers: {
    // Authorization: 'Bearer {token}'
  }
})

export const HTTP1 = axios.create({
  baseURL: `https://esslite-app.herokuapp.com/`,
  // baseURL: `http://localhost:8081/`,
  headers: {
    // Authorization: 'Bearer {token}'
  }
})