import { createStore } from 'vuex'
import { employeeData } from '@/models/employeeData'

export default createStore({
  state: {
    employeeid: 'tyss121',
    employeeData: employeeData,
    university: 'VTU',
    showElements: {
      showBranch: true,
      showSummary: true,
      showLanguages: true,
      showFrameworks: true,
      showRDBMS: true,
      showOthers: true,
      showProject: true,
      showAchievements: true,
      showEducation: true,
    },
    showList: {
      LanguagesList: [],
      AchievementsList: [],
    },
    eduDetails: '',
    showEduItems: [],
    projectItems: [],
    showCertItems: [],
    summaryList: [],
    experience: {
      totalExp: '',
      relExp: '',
      totalMonth: '',
      relMonth: '',
    },
    display: {
      dispEdu: true,
      dispCert: true,
      dispProject: true,
    },
    validateProfile: {
      first: true,
      last: true,
    }
  },
  mutations: {
    updateEmployeeData(state, payload) {
      state.employeeData[payload.type] = payload.value
    },
    updateEmployeeEduData(state, payload) {
      state.eduDetails = payload.value
    },
    updateShowEduItems(state, payload) {
      state.showEduItems = payload.value
    },
    updateProjectItems(state, payload) {
      state.projectItems = payload.value
    },
    updateCertItems(state, payload) {
      state.showCertItems = payload.value
    },
    updateSummaryItems(state, payload) {
      state.summaryList = payload.value
    },
    updateUniversity(state, payload) {
      state.university = payload.value
    },
  },
  actions: {
    updateEmployeeData(context, payload) {
      context.commit('updateEmployeeData', payload)
    },
    updateEmployeeEduData(context, payload) {
      context.commit('updateEmployeeEduData', payload)
    },
    updateShowEduItems(context, payload) {
      context.commit('updateShowEduItems', payload)
    },
    updateProjectItems(context, payload) {
      context.commit('updateProjectItems', payload)
    },
    updateCertItems(context, payload) {
      context.commit('updateCertItems', payload)
    },
    updateSummaryItems(context, payload) {
      context.commit('updateSummaryItems', payload)
    },
    updateUniversity(context, payload) {
      context.commit('updateUniversity', payload)
    },
  },
  modules: {
  }
})
