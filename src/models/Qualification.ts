export const qualification = [
    {
        name: '10th',
        value: '10th'
    },
    {
        name: '12th',
        value: '12th'
    },
    {
        name: 'UnderGraduate',
        value: 'UnderGraduate'
    },
    {
        name: 'PostGraduate',
        value: 'PostGraduate'
    }
]