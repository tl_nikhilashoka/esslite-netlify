export const employeeData = {
  personalDetails: {
    employeeid: "",
    firstname: "",
    lastname: "",
    designation: "",
    gender: "",
    joiningdate: "",
    contactnumber: "",
    alternativenumber: "",
    emrgycontactnumber1: "",
    emrgycontactnumber2: "",
    officemailid: "",
    personalmailid: "",
    permanentaddrs: "",
    temporaryaddrs: "",
    totalExp: "4/3",
    relExp: "2/2",
    summary: "Summary",
  },
  educationDetails: [
    {
      educationalid: 53,
      qualification: "BE",
      course: "ISE",
      specialization: "ML",
      insitution: "RNSIT",
      passoutyear: 2020,
      percentage: 55,
      employeeid: "tyss123",
    },
  ],
  technologyDetails: {
    stack: "FrontEnd",
    skills: [
      {
        type: "Languages",
        items: [
          {
            skillName: "Java",
            ratings: 4,
          },
          {
            skillName: "Python",
            ratings: 2,
          },
        ],
      },
      {
        type: "Frameworks - Front End",
        items: [
          {
            skillName: "Bootstrap",
            ratings: 4,
          },
          {
            skillName: "Materialize",
            ratings: 2,
          },
        ],
      },
      {
        type: "RDBMS Applications",
        items: [
          {
            skillName: "SQL",
            ratings: 4,
          },
          {
            skillName: "Oracle",
            ratings: 5,
          },
        ],
      },
      {
        type: "Other Tools",
        items: [
          {
            skillName: "Git",
            ratings: 4,
          },
          {
            skillName: "Andriod Studio",
            ratings: 5,
          },
        ],
      },
    ],
  },
  experienceDetails: [
    {
      designation: "Software Engineer",
      company: "asd",
      from: "Feb 2020",
      to: "Jun 2020",
      city: "Banglore",
    },
  ],
  projectDetails: [
    {
      name: "ESSlite",
      technology: [
        {
          type: "Front End Technologies",
          items: ["AngularJs"],
        },
        {
          type: "Back End Technologies",
          items: [
            "Hibernate with JPA",
            "Spring 5.0 (Core/IoC, MVC, DAO, REST, Boot)",
            "Project Lombok",
            "Log4J",
          ],
        },
        {
          type: "Design Patterns Used",
          items: [
            "Singleton",
            "Model View Controller (MVC)",
            "Data Transfer Object (DTO)",
            "Data Access Object (DAO) and Factory Pattern",
          ],
        },
        {
          type: "RDBMS Application",
          items: ["Oracle 10g and MySQL 5.5"],
        },
        {
          type: "Tools Used",
          items: ["Apache Tomcat 8.0"],
        },
      ],
      summary: "To maintain employee records",
      roles: "frontend developer",
      projectStart: "June, 2020",
      projectEnd: "Aug, 2020",
      teamSize: 4,
    }
  ],
  certificateDetails: [
    {
      certficateName: "Python",
      certifiedFrom: "abc",
      completion: "2018",
    },
  ],
  achievements: [
    {
      employee_id: "1",
      achievement_id: "1a",
      achievement_details: "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
    },
    {
      employee_id: "1",
      achievement_id: "1b",
      achievement_details: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.",
    },
  ],
};
