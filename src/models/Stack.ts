export const Stack = [
    {
        name: 'FrontEnd',
        value: 'FrontEnd'
    },
    {
        name: 'BackEnd',
        value: 'BackEnd'
    },
    {
        name: 'Full Stack',
        value: 'Full Stack'
    },
    {
        name: 'Others',
        value: 'Others'
    }
]