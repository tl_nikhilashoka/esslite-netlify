export const skillData = {
    stack: "frontend",
    skills: [{
        type: "Languages",
        items: [
            {
                skillName: "Java",
                ratings: 4
            },
            {
                skillName: "Python",
                ratings: 2
            }
        ]
    },
    {
        type: "Frameworks - Front End",
        items: [
            {
                skillName: "Bootstrap",
                ratings: 4
            },
            {
                skillName: "Materialize",
                ratings: 2
            }
        ]
    },
    {
        type: "RDBS Applications",
        items: [
            {
                skillName: "SQL",
                ratings: 4
            },
            {
                skillName: "Oracle",
                ratings: 5
            }
        ]
    },
    {
        type: "Other Tools",
        items: [
            {
                skillName: "Git",
                ratings: 4
            },
            {
                skillName: "Andriod Studio",
                ratings: 5
            }
        ]
    }
    ]
}

export const projectTechData = [
    {
        type: "Front End Technologies",
        items: [],
    },
    {
        type: "Back End Technologies",
        items: [],
    },
    {
        type: "RDBMS Application",
        items: [],
    },
    {
        type: "Tools Used",
        items: [],
    },
    {
        type: "Others",
        items: [],
    }
]


