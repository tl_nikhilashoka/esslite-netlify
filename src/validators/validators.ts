export const institutionalUniversityValidator = (value) => {
    if (/^[A-Za-z\s]{1,}[.]{0,1}[A-Za-z\s]{0,}$/.test(value)) {
        return true
      } else {
        return false
      }
}
export const percentageValidator = (value) => {
    if (parseFloat(value) >= 0 && parseFloat(value) < 101) {
        return true
    } else {
        return false
    }
}

export const nameValidator = (value) => {
    if (/^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$/.test(value)) {
        return true
    } else {
        return false
    }
}

export const numberValidator = (value) => {
    if (/^\d{10}$/.test(value)) {
        return true
    } else {
        return false
    }
}

export const lastNameValidator = (value) => {
    if (/^[a-zA-Z]*$/.test(value)) {
        return true
    } else {
        return false
    }
}

export const emailValidator = (value) => {
    if (/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/.test(value)) {
        return true
    } else {
        return false
    }
}

export const textValidator = (value) => {
    if (/^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$/.test(value)) {
        return true
    } else {
        return false
    }
}

export const empIDValidator = (value) => {
    if (/TY*/.test(value)) {
        return true
    } else {
        return false
    }
}

